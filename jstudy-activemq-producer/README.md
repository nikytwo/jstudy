MQ服务端测试，启动服务端监听
    1.先去http://activemq.apache.org/download.html下载MQ
    2.启动activemq/bin/win32/activemq.bat
    3.浏览器访问http://localhost:8161地址，账号密码都是admin
    4.访问成功打开activemq管理页面
    5.部署jstudy-activemq-consumer到tomcat
    6.执行TestMqMessageSender方法向consumer发送消息
    7.看到jstudy-activemq-consumer应用控制台打印jstudy-activemq-producer发送的Hello Wrold消息及表示成功