package cn.spream.jstudy.designpattern.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午3:28
 * To change this template use File | Settings | File Templates.
 */
public class FreightCar implements Car {

    @Override
    public void run() {
        System.out.println("大货车开动了");
    }

}
