package cn.spream.jstudy.designpattern.state;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午3:28
 * To change this template use File | Settings | File Templates.
 */
public class Context {

    private State state;

    public Context(State state) {
        this.state = state;
    }

    public void run(){
        String value = state.getValue();
        if("start".equals(value)){
            state.start();
        }else if("stop".equals(value)){
            state.stop();
        }else{

        }
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
