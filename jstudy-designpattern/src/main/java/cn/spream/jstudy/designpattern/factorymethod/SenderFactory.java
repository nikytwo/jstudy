package cn.spream.jstudy.designpattern.factorymethod;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午1:52
 * To change this template use File | Settings | File Templates.
 */
public class SenderFactory {

    public static Sender produce(SenderEnum senderEnum) {
        Sender sender = null;
        switch (senderEnum) {
            case MAIL:
                sender = new MailSender();
                break;
            case SMS:
                sender = new SmsSender();
                break;
            default:
                throw new RuntimeException("不存在此类型的sender,senderEnum=" + senderEnum);
        }
        return sender;
    }

}
