package cn.spream.jstudy.designpattern.strategy;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午3:35
 * To change this template use File | Settings | File Templates.
 */
public class Minus extends AbstractCalculator {

    @Override
    public int calculate(String exp) {
        int arrayInt[] = split(exp, "\\-");
        return arrayInt[0] - arrayInt[1];
    }

}
