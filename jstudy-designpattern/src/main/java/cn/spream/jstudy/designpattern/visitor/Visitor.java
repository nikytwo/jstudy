package cn.spream.jstudy.designpattern.visitor;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午4:00
 * To change this template use File | Settings | File Templates.
 */
public class Visitor {

    public void process(Service service) {
        System.out.println("基本业务");
    }

    public void process(Saving service) {
        System.out.println("存款");
    }

    public void process(Draw service) {
        System.out.println("提款");
    }

    public void process(Fund service) {
        System.out.println("基金");
    }

}
