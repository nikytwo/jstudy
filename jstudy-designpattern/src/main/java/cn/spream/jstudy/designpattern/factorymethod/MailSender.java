package cn.spream.jstudy.designpattern.factorymethod;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午1:50
 * To change this template use File | Settings | File Templates.
 */
public class MailSender implements Sender {

    @Override
    public boolean send(String info) {
        System.out.println("发送邮件：" + info);
        return true;
    }

}
