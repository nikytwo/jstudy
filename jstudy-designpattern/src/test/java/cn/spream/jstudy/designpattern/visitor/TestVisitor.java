package cn.spream.jstudy.designpattern.visitor;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午5:01
 * To change this template use File | Settings | File Templates.
 */
public class TestVisitor {

    @Test
    public void test(){
        Visitor visitor = new Visitor();
        visitor.process(new Saving());
        visitor.process(new Fund());
        visitor.process(new Draw());
    }

}
