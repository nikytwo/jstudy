package cn.spream.jstudy.designpattern.facade;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午2:40
 * To change this template use File | Settings | File Templates.
 */
public class TestComputer {

    @Test
    public void test(){
        Computer computer = new Computer();
        computer.startup();
        computer.shutdown();
    }

}
