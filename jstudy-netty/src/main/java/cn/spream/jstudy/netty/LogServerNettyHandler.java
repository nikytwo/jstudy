package cn.spream.jstudy.netty;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by sjx on 2015/11/6.
 */
@ChannelHandler.Sharable
public class LogServerNettyHandler extends SimpleChannelInboundHandler<Message> {

    private final static Log LOG = LogFactory.getLog(LogServerNettyHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Message msg) throws Exception {
        long id = msg.getId();
        String key = new String(msg.getKey(), "UTF-8");
        String value = new String(msg.getValue(), "UTF-8");
        LOG.debug("log server receive request message{id : " + id + ", key : " + key + ", value : " + value + "}");
        Message responseMessage = new Message();
        responseMessage.setId(id);
        responseMessage.setKey("server".getBytes("UTF-8"));
        responseMessage.setValue(msg.getValue());
        ctx.writeAndFlush(responseMessage);
    }

}
