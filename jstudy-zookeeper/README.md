## 一、下载Zookeeper ##
     http://www.apache.org/dyn/closer.cgi/zookeeper/
## 二、安装Zookeeper ##
     将下载下来的压缩包解压到自己想要的安装目录
## 三、配置Zookeeper ##
1. 进入/zookeeper-3.4.6/conf目录将zoo_sample.cfg改名为zoo.cfg

2. 打开改名后的zoo.cfg修改配置信息：
	- **tickTime=2000**
		<p>这个时间是作为Zookeeper服务器之间或客户端与服务器之间维持心跳的时间间隔,也就是每个 tickTime 时间就会发送一个心跳</p>
	- **dataDir=F:/zookeeper/data**
	  	<p>顾名思义就是Zookeeper保存数据的目录,默认情况下Zookeeper将写数据的日志文件也保存在这个目录里</p>
	- **dataLogDir=F:/zookeeper/log**
	  	<p>日志保存路径,这个要自己新建</p>
	- **clientPort=2181**
	  	<p>这个端口就是客户端连接Zookeeper服务器的端口,Zookeeper会监听这个端口接受客户端的访问请求</p>
	- **initLimit=5**
		<p>这个配置项是用来配置Zookeeper接受客户端（这里所说的客户端不是用户连接Zookeeper服务器的客户端,而是Zookeeper服务器集群中连接到Leader的Follower服务器）初始化连接时最长能忍受多少个心跳时间间隔数。当已经超过10个心跳的时间（也就是tickTime）长度后Zookeeper服务器还没有收到客户端的返回信息，那么表明这个客户端连接失败。总的时间长度就是5*2000=10秒</p>
	- **syncLimit=2**
	    <p>这个配置项标识Leader与Follower之间发送消息,请求和应答时间长度最长不能超过多少个tickTime的时间长度,总的时间长度就是2*2000=4秒</p>
	- **server.1=192.168.1.1:2888:3888<br/>server.2=192.168.1.2:2888:3888<br/>server.A=B:C:D**
		<p>其中A是一个数字,表示这个是第几号服务器;B是这个服务器的ip地址;C表示的是这个服务器与集群中的Leader服务器交换信息的端口;
	    D表示的是万一集群中的Leader服务器挂了,需要一个端口来重新进行选举选出一个新的Leader,而这个端口就是用来执行选举时服务器相互通信的端口.如果是伪集群的配置方式,由于B都是一样,所以不同的Zookeeper实例通信端口号不能一样,所以要给它们分配不同的端口号.</p>
	- **添加myid文件**
	    <p>除了修改zoo.cfg配置文件,集群模式下还要配置一个文件myid,这个文件在dataDir目录下,这个文件里面就有一个数据就是A的值,Zookeeper启动时会读取这个文件,拿到里面的数据与zoo.cfg里面的配置信息比较从而判断到底是那个server.</p>

## 四、启动Zookeeper ##
    进入到zookeeper安装目录下的bin目录，执行zkServer.cmd启动zookeeper
## 五、客户端 ##
    进入到zookeeper安装目录下的bin目录，执行zkCli.cmd -server 127.0.0.1:2181连接指定的服务端
	使用create /test "abc" 来创建一个目录test里面存储了字符abc
