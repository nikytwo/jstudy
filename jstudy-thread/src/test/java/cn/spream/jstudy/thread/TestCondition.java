package cn.spream.jstudy.thread;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-7-2
 * Time: 下午5:03
 * To change this template use File | Settings | File Templates.
 */
public class TestCondition {

    @Test
    public void test() throws InterruptedException {
        final BoundedBuffer boundedBuffer = new BoundedBuffer();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(true){
                    try {
                        System.out.println("→准备放入用户");
                        User user = new User(String.valueOf(i++));
                        boundedBuffer.put(user);
                        System.out.println("↓放入用户成功：" + user.getName());
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    try {
                        System.out.println("←准备取出用户");
                        User user = (User) boundedBuffer.take();
                        System.out.println("↑取出用户：" + user.getName());
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }).start();
        Thread.sleep(500000);
    }

    private class User{
        private String name;

        private User(String name) {
            this.name = name;
        }

        String getName() {
            return name;
        }

        void setName(String name) {
            this.name = name;
        }
    }

}
