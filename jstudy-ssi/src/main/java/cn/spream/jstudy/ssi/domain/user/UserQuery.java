package cn.spream.jstudy.ssi.domain.user;

import cn.spream.jstudy.ssi.domain.BaseQuery;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-21
 * Time: 中午12:49
 * To change this template use File | Settings | File Templates.
 */
public class UserQuery extends BaseQuery{

    private long id;
    private String name;
    private int sex = -1;
    private int age = -1;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "UserQuery{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }
}
